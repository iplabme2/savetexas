import Foundation
import MapKit
import Contacts

class Demolished: NSObject, MKAnnotation {
  let title: String?
  let classification: String?
  let coordinate: CLLocationCoordinate2D
  
  init(
    title: String?,
    classification: String?,
    coordinate: CLLocationCoordinate2D
  ) {
    self.title = title
    self.classification = classification
    self.coordinate = coordinate
    super.init()
  }
  
  init?(feature: MKGeoJSONFeature) {
    guard
      let point = feature.geometry.first as? MKPointAnnotation,
      let propertiesData = feature.properties,
      let json = try? JSONSerialization.jsonObject(with: propertiesData),
      let properties = json as? [String: Any]
      else {
        return nil
    }
    
    title = properties["title"] as? String
    classification = properties["classification"] as? String
    coordinate = point.coordinate
    super.init()
  }
  
  var subtitle: String? {
    return ""
  }
  
  var mapItem: MKMapItem? {
    let addressDict = [CNPostalAddressStreetKey: ""]
    let placemark = MKPlacemark(
      coordinate: coordinate,
      addressDictionary: addressDict)
    let mapItem = MKMapItem(placemark: placemark)
    mapItem.name = title
    return mapItem
  }
  
  var markerTintColor: UIColor  {
    switch classification {
    case "hydrant":
      return .red
    case "power_pylon":
      return .cyan
    case "mailbox":
      return .blue
    case "tree":
      return .purple
    default:
      return .green
    }
  }
  
  var image: UIImage {
    guard let name = classification else { return #imageLiteral(resourceName: "tree") }
    
    switch name {
    case "hydrant":
      return #imageLiteral(resourceName: "hydrant")
    case "power_pylon":
      return #imageLiteral(resourceName: "power_line")
    case "mailbox":
      return #imageLiteral(resourceName: "mailbox")
    case "tree":
      return #imageLiteral(resourceName: "tree")
    case "streetlight":
      return #imageLiteral(resourceName: "streetlight")
    default:
      return #imageLiteral(resourceName: "tree")
    }
  }
}
