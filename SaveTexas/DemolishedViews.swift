import Foundation
import MapKit

class DemolishedMarkerView: MKMarkerAnnotationView {
  override var annotation: MKAnnotation? {
    willSet {
      guard let demolished = newValue as? Demolished else {
        return
      }
      canShowCallout = true
      calloutOffset = CGPoint(x: -5, y: 5)
      rightCalloutAccessoryView = UIButton(type: .detailDisclosure)

      markerTintColor = demolished.markerTintColor
      glyphImage = demolished.image
    }
  }
}

class DemolishedView: MKAnnotationView {
  override var annotation: MKAnnotation? {
    willSet {
      guard let demolished = newValue as? Demolished else {
        return
      }

      canShowCallout = false
      image = demolished.image
      clusteringIdentifier = demolished.classification
    }
  }
}
