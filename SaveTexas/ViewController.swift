import UIKit
import MapKit
import Floaty
import MessageUI

class ViewController: UIViewController, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate {
  @IBOutlet private var mapView: MKMapView!
  private var demolishes: [Demolished] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let initialLocation = CLLocation(latitude: 30.2764013, longitude: -97.7473542)
    mapView.centerToLocation(initialLocation)
    
    let austinCenter = CLLocation(latitude: 30.2764013, longitude: -97.7473542)
    let region = MKCoordinateRegion(
      center: austinCenter.coordinate,
      latitudinalMeters: 2000000,
      longitudinalMeters: 2000000)
    mapView.setCameraBoundary(
      MKMapView.CameraBoundary(coordinateRegion: region),
      animated: true)
    
    let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 3000000)
    mapView.setCameraZoomRange(zoomRange, animated: true)
    
    mapView.delegate = self
    
    mapView.register(
      DemolishedView.self,
      forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
    
    loadInitialData()
    mapView.addAnnotations(demolishes)
    let gestureRecognizer = UITapGestureRecognizer(
      target: self, action:#selector(handleTap))
    gestureRecognizer.delegate = self
    mapView.addGestureRecognizer(gestureRecognizer)
    
    let floaty = Floaty()
    floaty.addItem("Send points via email...", icon: UIImage(named: "email")!, handler: { item in
      let alert = UIAlertController(title: "Enter email address", message: "", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Send points", style: .default, handler: {_ in
        let textField = alert.textFields![0] as UITextField
        if MFMailComposeViewController.canSendMail() {
          let mail = MFMailComposeViewController()
          mail.mailComposeDelegate = self
          mail.setToRecipients([textField.text!])
          mail.setSubject("Save Texas")
          mail.setMessageBody(self.makeJSONData() ?? "", isHTML: false)
          
          self.present(mail, animated: true)
        } else {
          
        }
      }))
      alert.addTextField { (textField) in
        textField.placeholder = "addr@mail.com"
      }
      self.present(alert, animated: true, completion: nil)
      floaty.close()
    })
    self.view.addSubview(floaty)
  }
  
  func makeJSONData() -> String? {
    do {
      let jsonString = try demolishes.map({ aw in
        var topDict = Dictionary<String, Any > ()
        var coordDict = Dictionary<String, Double> ()
        var objectArray = Array<Dictionary<String, Any> > ()
        coordDict["lat"] = aw.coordinate.latitude
        coordDict["lon"] = aw.coordinate.longitude
        topDict["coordinates"] = coordDict
        var objectDict = Dictionary<String, Any> ()
        objectDict["type"] = aw.classification
        objectDict["count"] = 1
        objectArray.append(["object" : objectDict])
        topDict["objects"] = objectArray
        let jsonData = try JSONSerialization.data(withJSONObject: topDict, options: [])
        return String(data: jsonData, encoding: .utf8)!
      }).reduce(into: "") { partialResult, s in
        partialResult = partialResult + s + ","
      }
      
      return String(jsonString.dropLast())
    } catch {
        print(error)
    }
    return nil
  }
  
  func saveJSONGeoData() {
    do {
      let fileURL = try FileManager.default
        .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        .appendingPathComponent("points.geojson")
      do {
      try FileManager.default.removeItem(at: fileURL)
      } catch {
        print(error)
      }
      
      var jsonString = try demolishes.map({ aw in
        var featureDict = Dictionary<String, Any> ()
        var propDict = Dictionary<String, Any> ()
        propDict["title"] = aw.title
        propDict["classification"] = aw.classification
        featureDict["type"] = "Feature"
        featureDict["properties"] = propDict
        var coordDict = Dictionary<String, Any> ()
        coordDict["type"] = "Point"
        coordDict["coordinates"] = [aw.coordinate.longitude, aw.coordinate.latitude]
        featureDict["geometry"] = coordDict
        let jsonData = try JSONSerialization.data(withJSONObject: featureDict, options: [])
        return String(data: jsonData, encoding: .utf8)!
      }).reduce(into: "") { partialResult, s in
        partialResult = partialResult + s + ","
      }
      jsonString = "{\"type\": \"FeatureCollection\", \"features\": [ " + jsonString.dropLast() + " ] }\n"
      try jsonString.dropLast().write(to: fileURL, atomically: false, encoding: .utf8)
    } catch {
      print(error)
    }
  }
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true)
  }
  
  @objc private func handleTap(gestureRecognizer: UITapGestureRecognizer) {
    
    let location = gestureRecognizer.location(in: mapView)
    let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
    
    showAlert(coordinate: coordinate)
    
  }
  
  @IBAction func showAlert(coordinate: CLLocationCoordinate2D) {
    let alert = UIAlertController(title: "Add damaged object", message: "Please select type", preferredStyle: .actionSheet)
    
    alert.addAction(UIAlertAction(title: "Power Pylon", style: .default , handler:{ (UIAlertAction)in
      let annotation = Demolished(title: "Power Pylon", classification: "power_pylon", coordinate: coordinate)
      self.mapView.addAnnotation(annotation)
      self.demolishes.append(annotation)
      self.saveJSONGeoData()
    }))
    
    alert.addAction(UIAlertAction(title: "Streetlight", style: .default , handler:{ (UIAlertAction)in
      let annotation = Demolished(title: "Streetlight", classification: "streetlight", coordinate: coordinate)
      self.mapView.addAnnotation(annotation)
      self.demolishes.append(annotation)
      self.saveJSONGeoData()
    }))
    
    alert.addAction(UIAlertAction(title: "Tree", style: .default , handler:{ (UIAlertAction)in
      let annotation = Demolished(title: "Tree", classification: "tree", coordinate: coordinate)
      self.mapView.addAnnotation(annotation)
      self.demolishes.append(annotation)
     self.saveJSONGeoData()
    }))
    
    alert.addAction(UIAlertAction(title: "Mailbox", style: .default , handler:{ (UIAlertAction)in
      let annotation = Demolished(title: "Mailbox", classification: "mailbox", coordinate: coordinate)
      self.mapView.addAnnotation(annotation)
      self.demolishes.append(annotation)
      self.saveJSONGeoData()
    }))
    
    alert.addAction(UIAlertAction(title: "Hydrant", style: .default , handler:{ (UIAlertAction)in
      let annotation = Demolished(title: "Hydrant", classification: "hydrant", coordinate: coordinate)
      self.mapView.addAnnotation(annotation)
      self.demolishes.append(annotation)
     self.saveJSONGeoData()
    }))
    
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
    }))
    
    self.present(alert, animated: true, completion: {
      
    })
  }
  private func loadInitialData() {
    guard
      let fileURL = try? FileManager.default
          .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
          .appendingPathComponent("points.geojson"),
      let demolishedData = try? Data(contentsOf: fileURL)
    else {
      return
    }
    
    do {
      let features = try MKGeoJSONDecoder()
        .decode(demolishedData)
        .compactMap { $0 as? MKGeoJSONFeature }
      let validWorks = features.compactMap(Demolished.init)
      demolishes.append(contentsOf: validWorks)
    } catch {
      print("Unexpected error: \(error).")
    }
  }
}

private extension MKMapView {
  func centerToLocation(_ location: CLLocation, regionRadius: CLLocationDistance = 1000) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}

extension ViewController: MKMapViewDelegate {
  func mapView(
    _ mapView: MKMapView,
    annotationView view: MKAnnotationView,
    calloutAccessoryControlTapped control: UIControl
  ) {
    guard let demolished = view.annotation as? Demolished else {
      return
    }
    
    let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
    demolished.mapItem?.openInMaps(launchOptions: launchOptions)
  }
}
